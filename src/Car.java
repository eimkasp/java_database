import com.mysql.jdbc.Statement;

import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Car extends Database  implements  DataManagement{
    private static int carsCount = 0;
    public static Car[] cars = new Car[10];

    private int id;
    private String numberPlate;

    private String carMake;
    private int userId;
    private User user;
    private DataManagement test;


    Car(String numberPlate, String carMake) {
        super();
        this.numberPlate = numberPlate;
        this.carMake = carMake;
        Car.carsCount++;
        System.out.println("Sukurta nauja masina: " + this.carMake);
        this.insertData();
    }

    @Override
    public int insertData() {
        try {
            String insertQueryStatement = "INSERT INTO `nuoma_cars` (`number_plate`, `car_make`) VALUES (?, ?);";
            dbPrepareStatement = dbConnection.prepareStatement(insertQueryStatement,  Statement.RETURN_GENERATED_KEYS);
            dbPrepareStatement.setString(1, this.numberPlate);
            dbPrepareStatement.setString(2, this.carMake);
            // execute insert SQL statement
            dbPrepareStatement.execute();

            try (ResultSet generatedKeys = dbPrepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    this.id = generatedKeys.getInt(1);
                }
                else {
                    throw new SQLException("Creating car failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            System.out.print("Kazkas blogai");
            e.printStackTrace();
        }

        return this.id;
    }



    Car(int id, String numberPlate, String carMake) {
        super();
        this.id = id;
        this.numberPlate = numberPlate;
        this.carMake = carMake;
        Car.carsCount++;
        System.out.println("Sukurta nauja masina: " + this.carMake);
    }

    Car(int id, String numberPlate, String carMake, User user) {
        super();
        this.id = id;
        this.numberPlate = numberPlate;
        this.carMake = carMake;
        this.user = user;
        Car.carsCount++;
        System.out.println("Sukurta nauja masina: " + this.carMake);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static int getCarsCount() {
        return carsCount;
    }

    public static void setCarsCount(int carsCount) {
        Car.carsCount = carsCount;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
//        this.saveData();
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void saveData() {
        try {
            String insertQueryStatement = "UPDATE `nuoma_cars` SET `car_make` = ?, `number_plate` = ?, `user_id` = ? WHERE `nuoma_cars`.`id` = ?;";
            dbPrepareStatement = dbConnection.prepareStatement(insertQueryStatement);
            dbPrepareStatement.setString(1, this.carMake);
            dbPrepareStatement.setString(2, this.numberPlate);
            dbPrepareStatement.setInt(3, this.userId);
            dbPrepareStatement.setInt(4, this.id);

            // execute insert SQL statement
            dbPrepareStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.print("Kazkas blogai");
            e.printStackTrace();
        }
    }

    public void deleteData() {
        try {
            String insertQueryStatement = "DELETE FROM `nuoma_cars` WHERE `nuoma_cars`.`id` = ?;";
            dbPrepareStatement = dbConnection.prepareStatement(insertQueryStatement);
            dbPrepareStatement.setInt(1, this.id);
            // execute insert SQL statement
            dbPrepareStatement.execute();
            int deletedIndex = -1;
            for(int i = 0; i < Car.getCarsCount(); i++) {
                if(Car.cars[i].id == this.id) {
                    Car.cars[i] = null;
                    deletedIndex = i;
                }
            }


            /* Istriname tuscia null objekta is masyvo, ir perstumiame visus elementus per viena vieta */
            for(int i = deletedIndex; i < Car.getCarsCount() - 1; i++) {
                Car.cars[i] = Car.cars[i + 1];
            }
            Car.carsCount--;
            Car.cars[Car.getCarsCount()]  = null;

        } catch (SQLException e) {
            System.out.print("Kazkas blogai");
            e.printStackTrace();
        }
    }

    public static void getData() {
        try {
            String selectQueryStatement = "SELECT * from nuoma_cars";
            dbPrepareStatement = dbConnection.prepareStatement(selectQueryStatement);
            ResultSet results = dbPrepareStatement.executeQuery();

            while (results.next()) {
                /* Gauname rezultatus is duombazes ir issaugome i laikinus darbinius kintamuosius */
                int id = results.getInt("id");
                String number = results.getString("number_plate");
                String carMake =  results.getString("car_make");
                Integer userId =  results.getInt("user_id");

                /* Sukuriame masinos objekta */
                if(userId != null) {
                    User temp = User.getUserById(userId);
                    Car.cars[Car.getCarsCount()] = new Car(id, number, carMake, temp);

                } else {
                    Car.cars[Car.getCarsCount()] = new Car(id, number, carMake);

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
